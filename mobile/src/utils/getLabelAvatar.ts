import { IOwner } from '../models/owner'

export const getLabelAvatar = (user: IOwner): string => {
    return `${user.firstName?.charAt(0)}${user.lastName?.charAt(0)}`.toUpperCase()
}