import { IOwner, Sorting } from '../models/owner'

export const getSortedList = (list: IOwner[], sortBy: Sorting): IOwner[] => {
    const newList = [...list]
    if (sortBy === 'Name') return newList.sort((a: any, b: any) => a.name.localeCompare(b.name))
    return newList.sort((a: any, b: any) => b.cats.length - a.cats.length)
}