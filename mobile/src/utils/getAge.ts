export const getAge = (age: number): string => {
    const diff = age - Math.floor(age)
    const month = Math.floor(((diff * 100) / 100) * 12)
    const year = Math.floor(age)
    return `${year} Years ${month} months`
}