import { OwnerPayload } from './owner'

export type ActionMap<M extends { [index: string]: any }> = {
    [Key in keyof M]: M[Key] extends undefined
      ? {
          type: Key
        }
      : {
          type: Key
          payload: Payload<M[Key]>
        }
}

export type Payload<Type> = {
  [P in keyof Type]+?: Type[P]
}

export type OwnerActions = ActionMap<OwnerPayload>[keyof ActionMap<OwnerPayload>]

export enum OwnerActionTypes {
  GET_OWNERS = 'GET_OWNERS',
  GET_OWNER_DETAIL = 'GET_OWNER_DETAIL',
  GET_MASTER_DETAIL = 'GET_MASTER_DETAIL',
  SET_MASTER_DETAIL = 'SET_MASTER_DETAIL',
}
