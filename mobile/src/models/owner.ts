
import {OwnerActionTypes} from './action'

export interface ICat {
    id: string
    name: string
    age?: Nullable<number>
    owner?: Nullable<IOwner>
}

export interface IOwner {
    id: string
    name?: Nullable<string>
    firstName: string
    lastName: string
    age?: Nullable<number>
    master: boolean
    cats?: Nullable<ICat[]>
}

export type Nullable<T> = T | null | undefined
export type Sorting = 'Name' | 'Number of Cats'

export interface OwnerStateType {
    owners: IOwner[]
    owner: Nullable<IOwner>
    master: Nullable<IOwner>
    loadingOwners: boolean
    loadingOwner: boolean
    loadingMaster: boolean
    errorOwners: Nullable<string>
    errorOwner: Nullable<string>
    errorMaster: Nullable<string>,
    sortBy: Sorting
}

export type OwnerPayload = {
    [key in OwnerActionTypes]: OwnerStateType
}

export interface OwnerContextActions {    
    getOwners: (sortBy: Sorting) => Promise<void>
    getOwner: (id: number) => Promise<void>
    getMaster: () => Promise<void>
    setMaster: (id: number) => Promise<void>
}

export interface OwnerContextType extends OwnerStateType, OwnerContextActions { }
