import React, {ComponentPropsWithRef} from 'react'
import {View, Image, TouchableOpacity} from 'react-native'

import DetailButtonStyle from '../../styles/buttons/DetailButton'

export type Props = ComponentPropsWithRef<typeof View> & {
  actionButton: () => void
}

const DetailButton = ({
  actionButton,
}: Props) => {
  return (
    <TouchableOpacity
      style={DetailButtonStyle.rowEnd}
      activeOpacity={0.9}
      onPress={actionButton}>
      <Image
        style={DetailButtonStyle.iconDetail}
        source={require('./../../assets/icons/arrow-right.png')}
      />
    </TouchableOpacity>
  )
}

export default DetailButton
