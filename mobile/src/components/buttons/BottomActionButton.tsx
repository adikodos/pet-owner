import React, {ComponentPropsWithRef} from 'react'
import {View, Text, TouchableOpacity} from 'react-native'

import BottomActionButtonStyle from '../../styles/buttons/BottomActionButton'

export type Props = ComponentPropsWithRef<typeof View> & {
  text: string
  onPress: () => void
}

const BottomActionButton = ({text, onPress}: Props) => {
  return (
    <TouchableOpacity
      style={BottomActionButtonStyle.btnContainer}
      onPress={onPress}>
      <View style={BottomActionButtonStyle.btnAction}>
        <Text style={BottomActionButtonStyle.btnText}>{text}</Text>
      </View>
    </TouchableOpacity>
  )
}

export default BottomActionButton
