import React, {ComponentPropsWithRef} from 'react'
import {View, Image, TouchableOpacity} from 'react-native'

import FavoriteButtonStyle from '../../styles/buttons/FavoriteButton'

export type Props = ComponentPropsWithRef<typeof View> & {
  isFavorite: boolean
  onClickFavorite: () => void
}

const FavoriteButton = ({
  isFavorite,
  onClickFavorite,
}: Props) => {
  const imgSource = isFavorite
    ? require('./../../assets/icons/favorite-fill.png')
    : require('./../../assets/icons/favorite-outline.png')

  return (
    <TouchableOpacity
      style={FavoriteButtonStyle.rowEnd}
      activeOpacity={0.9}
      onPress={onClickFavorite}>
      <Image style={FavoriteButtonStyle.iconDetail} source={imgSource} />
    </TouchableOpacity>
  )
}

export default FavoriteButton
