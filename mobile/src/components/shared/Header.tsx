import {NavigationContext} from '@react-navigation/native'
import React, {
  ComponentPropsWithRef,
  useContext,
  useEffect,
  useState,
} from 'react'
import {View, Text, Image, TouchableOpacity, ViewStyle} from 'react-native'
import { useOwnerContext } from '../../contexts/OwnerContext'
import HeaderStyle from '../../styles/shared/Header'
import { getLabelAvatar } from '../../utils/getLabelAvatar'

import AvatarText from '../avatar/AvatarText'

export type Props = ComponentPropsWithRef<typeof View> & {
  showBackButton?: boolean
}

const Header = ({showBackButton = false}: Props) => {
  const { master, loadingMaster } = useOwnerContext()
  const [headerText, setHeaderText] = useState<string>('')
  const [labelAvatar, setLabelAvatar] = useState<string>('')
  const navigation = useContext(NavigationContext)

  const gapHeader: ViewStyle = showBackButton ? {marginRight: 74} : {}

  useEffect(() => {
    if (master) {
      setHeaderLabel()
    }
  },[])

  useEffect(() => {
    if (master && !loadingMaster) {
      setHeaderLabel()
    }
  }, [master, loadingMaster])

  const setHeaderLabel = () => {
    setHeaderText(`Master: ${master!.name}`)
    setLabelAvatar(getLabelAvatar(master!))
  }

  const onBackNavigate = () => {
    navigation?.goBack()
  }

  return (
    <View style={HeaderStyle.container}>
      {!loadingMaster && showBackButton && (
        <TouchableOpacity
          style={HeaderStyle.backButtonContainer}
          onPress={onBackNavigate}>
          <Image
            style={HeaderStyle.imageBackButton}
            source={require('./../../assets/icons/arrow-back.png')}
          />
        </TouchableOpacity>
      )}

      {!loadingMaster && (
        <View style={[HeaderStyle.headerTitleContainer, gapHeader]}>
          <AvatarText
            label={labelAvatar}
            size={40}
            labelStyle={HeaderStyle.avatarLabel}
            style={HeaderStyle.avatarContainer}
          />
          <Text style={HeaderStyle.headerTitle} numberOfLines={1}>
            {headerText}
          </Text>
        </View>
      )}
    </View>
  )
}

export default Header
