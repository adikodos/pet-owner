import React, {ComponentPropsWithRef} from 'react'
import {View, Text, TouchableOpacity, Alert} from 'react-native'
import {ICat} from '../../models/owner'
import OwnerListStyle from '../../styles/lists/OwnerList'

import PetListStyle from '../../styles/lists/PetList'
import { getAge } from '../../utils/getAge'
import DetailButton from '../buttons/DetailButton'

export type Props = ComponentPropsWithRef<typeof View> & {
  cat: ICat
}

const PetList = ({cat}: Props) => {
  const catAge: string = getAge(cat.age!)

  const onClickItem = () => {
    Alert.alert('Cat Selected', `Name: ${cat.name}, Age: ${catAge}, ID: ${cat.id}`)
  }

  return (
    <View style={[PetListStyle.listPet, OwnerListStyle.boxShadow]}>
      <TouchableOpacity
        style={PetListStyle.petCard}
        activeOpacity={0.9}
        onPress={onClickItem}>
        <View style={PetListStyle.titlePet}>
          <Text style={PetListStyle.petText} numberOfLines={1}>
            {cat.name}
          </Text>
          <Text style={PetListStyle.petSubtext} numberOfLines={1}>
            Age: {catAge}
          </Text>
        </View>
      </TouchableOpacity>

      <DetailButton actionButton={onClickItem} />
    </View>
  )
}

export default PetList
