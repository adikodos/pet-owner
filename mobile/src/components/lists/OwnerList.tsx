import React from 'react'
import {View, Text, TouchableOpacity} from 'react-native'

import OwnerListStyle from '../../styles/lists/OwnerList'

import AvatarText from '../avatar/AvatarText'
import DetailButton from './../buttons/DetailButton'
import FavoriteButton from './../buttons/FavoriteButton'
import useFavorite from '../../hooks/UseFavorite'
import {IOwner} from '../../models/owner'
import { getLabelAvatar } from '../../utils/getLabelAvatar'

export type Props = React.ComponentPropsWithRef<typeof View> & {
  user: IOwner
  goToDetail: (id: string) => void
}

const OwnerList = ({user, goToDetail}: Props) => {
  const {favorite, onFavorite} = useFavorite(user)
  const labelAvatar = getLabelAvatar(user)

  return (
    <View style={[OwnerListStyle.listRow, OwnerListStyle.boxShadow]}>
      <AvatarText
        label={labelAvatar}
        size={40}
        labelStyle={OwnerListStyle.avatarLabel}
      />

      <TouchableOpacity
        style={OwnerListStyle.rowStart}
        activeOpacity={0.9}
        onPress={() => goToDetail(user.id)}>
        <View style={OwnerListStyle.titleOwner}>
          <Text style={OwnerListStyle.ownerText} numberOfLines={1}>
            {user.name}
          </Text>
        </View>
      </TouchableOpacity>

      <FavoriteButton isFavorite={favorite} onClickFavorite={onFavorite} />
      <DetailButton actionButton={() => goToDetail(user.id)} />
    </View>
  )
}

export default OwnerList
