import React, {
  ComponentPropsWithRef,
  forwardRef,
  useImperativeHandle,
  useState,
} from 'react'
import {
  View,
  Text,
  TouchableWithoutFeedback,
  Modal,
  FlatList,
  Image,
  TouchableHighlight,
} from 'react-native'

import BottomPopupStyle from '../../styles/modals/BottomPopup'
import { Sorting } from '../../models/owner'

export interface ListItem {
  id: string | number
  name: Sorting
  checked?: boolean
}

export type Props = ComponentPropsWithRef<typeof View> & {
  list: ListItem[]
  title: string
  onTouchOutside: () => void
  onSelectSort: (id: number | string) => void
}

const BottomPopup = forwardRef(
  (
    {
      list,
      title,
      onTouchOutside,
      onSelectSort,
    }: Props,
    ref,
  ) => {
    const [show, setShow] = useState<boolean>(false)

    useImperativeHandle(ref, () => ({
      show: () => setShow(true),
      hide: () => setShow(false),
    }))

    const onClose = () => setShow(false)

    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={show}
        onRequestClose={onClose}>
        <View style={BottomPopupStyle.modalContainer}>
          <TouchableOutside onTouch={onTouchOutside} />
          <View style={BottomPopupStyle.popupContainer}>
            <TitlePopup title={title} />
            <ContentPopup list={list} onSelect={onSelectSort} />
          </View>
        </View>
      </Modal>
    )
  },
)

const ContentPopup = ({
  list,
  onSelect,
}: {
  list: ListItem[]
  onSelect: (id: number | string) => void
}) => {
  return (
    <View>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={list}
        renderItem={({item}) => <ItemPopup item={item} onSelect={onSelect} />}
        extraData={list}
        keyExtractor={(item, index) => index.toString()}
        ItemSeparatorComponent={() => (
          <View style={BottomPopupStyle.separator} />
        )}
        contentContainerStyle={BottomPopupStyle.contentList}
      />
    </View>
  )
}

const TitlePopup = ({title}: {title: string}) => {
  return (
    <View style={BottomPopupStyle.titlePopupContainer}>
      <Text style={BottomPopupStyle.titlePopup}>{title}</Text>
    </View>
  )
}

const ItemPopup = ({
  item,
  onSelect,
}: {
  item: ListItem
  onSelect: (id: number | string) => void
}) => {
  const iconCheck = item.checked
    ? require('./../../assets/icons/checks.png')
    : require('./../../assets/icons/square.png')
  return (
    <TouchableHighlight
      onPress={() => onSelect(item.id)}
      underlayColor={'#F4F4F4'}
      style={BottomPopupStyle.itemTouchable}>
      <View style={BottomPopupStyle.itemContainer}>
        <Image source={iconCheck} style={BottomPopupStyle.itemImage} />
        <Text style={BottomPopupStyle.itemText}>{item.name}</Text>
      </View>
    </TouchableHighlight>
  )
}

const TouchableOutside = ({onTouch}: {onTouch: () => void}) => {
  const view = <View style={BottomPopupStyle.touchableView} />
  if (!onTouch) {
    return view
  }

  return (
    <TouchableWithoutFeedback
      onPress={onTouch}
      style={BottomPopupStyle.touchableView}>
      {view}
    </TouchableWithoutFeedback>
  )
}

export default BottomPopup
