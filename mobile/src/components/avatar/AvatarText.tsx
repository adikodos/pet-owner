import React from 'react'
import {
  View,
  ViewStyle,
  StyleSheet,
  StyleProp,
  TextStyle,
  useWindowDimensions,
  Text,
} from 'react-native'

import AvatarTextStyle from '../../styles/avatar/AvatarText'
import { ITEM_CONTAINER_HEIGHT } from '../../constants/appConstants'

const defaultSize = ITEM_CONTAINER_HEIGHT

export type Props = React.ComponentPropsWithRef<typeof View> & {
  label: string
  size?: number
  color?: string
  style?: StyleProp<ViewStyle>
  labelStyle?: StyleProp<TextStyle>
}

const AvatarText = ({
  label,
  size = defaultSize,
  style,
  labelStyle,
  color: customColor,
  ...rest
}: Props) => {
  const {backgroundColor = 'rgba(0, 0, 0, 0.2)', ...restStyle} =
    StyleSheet.flatten(style) || {}
  const textColor = customColor ?? '#FFFFFF'
  const {fontScale} = useWindowDimensions()

  return (
    <View
      style={[
        {
          width: size,
          height: size,
          borderRadius: size / 2,
          backgroundColor,
        },
        AvatarTextStyle.container,
        restStyle,
      ]}
      {...rest}>
      <Text
        style={[
          AvatarTextStyle.text,
          {
            color: textColor,
            fontSize: size / 2,
            lineHeight: size / fontScale,
          },
          labelStyle,
        ]}
        numberOfLines={1}>
        {label}
      </Text>
    </View>
  )
}

export default AvatarText
