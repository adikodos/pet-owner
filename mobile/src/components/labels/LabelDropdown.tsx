import React, {ComponentPropsWithRef} from 'react'
import {View, Image, Text, TouchableOpacity} from 'react-native'

import LabelDropDownStyle from '../../styles/labels/LabelDropDown'

export type Props = ComponentPropsWithRef<typeof View> & {
  label?: string
  selected: string
  onShowPopup: () => void
}

const LabelDropDown = ({label = 'Sort By', selected, onShowPopup}: Props) => {
  const labelText = `${label}: ${selected}`

  return (
    <TouchableOpacity
      onPress={onShowPopup}
      style={LabelDropDownStyle.container}>
      <Text style={LabelDropDownStyle.label}>{labelText}</Text>
      <Image
        style={LabelDropDownStyle.icon}
        source={require('./../../assets/icons/arrow-dropdown.png')}
      />
    </TouchableOpacity>
  )
}

export default LabelDropDown
