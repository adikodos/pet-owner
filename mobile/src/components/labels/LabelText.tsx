import React, {ComponentPropsWithRef} from 'react'
import {View, StyleProp, TextStyle, Text} from 'react-native'

import LabelTextStyle from '../../styles/labels/LabelText'

export type Props = ComponentPropsWithRef<typeof View> & {
  label: string
  labelStyle?: StyleProp<TextStyle>
}

const LabelText = ({label, labelStyle}: Props) => {
  return <Text style={[labelStyle, LabelTextStyle.labelText]}>{label}</Text>
}

export default LabelText
