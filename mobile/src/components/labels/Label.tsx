import React, {ComponentPropsWithRef} from 'react'
import {View, StyleProp, ViewStyle} from 'react-native'

import LabelStyle from '../../styles/labels/Label'

export type Props = ComponentPropsWithRef<typeof View> & {
  children: React.ReactNode
  style?: StyleProp<ViewStyle>
}

const Label = ({children, style}: Props) => {
  return <View style={[LabelStyle.container, style]}>{children}</View>
}

export default Label
