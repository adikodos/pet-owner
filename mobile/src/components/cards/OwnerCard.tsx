import React, {ComponentPropsWithRef} from 'react'
import {View, Text} from 'react-native'
import useFavorite from '../../hooks/UseFavorite'
import {IOwner} from '../../models/owner'
import OwnerListStyle from '../../styles/lists/OwnerList'
import OwnerCardStyle from '../../styles/cards/OwnerCard'
import AvatarText from '../avatar/AvatarText'
import FavoriteButton from '../buttons/FavoriteButton'
import { getLabelAvatar } from '../../utils/getLabelAvatar'

export type Props = ComponentPropsWithRef<typeof View> & {
  user: IOwner
}

const OwnerCard = ({user}: Props) => {
  const {favorite, onFavorite} = useFavorite(user)
  const labelAvatar = getLabelAvatar(user)
  return (
    <View style={[OwnerCardStyle.cardContainer, OwnerListStyle.boxShadow]}>
      <AvatarText
        label={labelAvatar}
        size={56}
        labelStyle={OwnerCardStyle.avatarLabel}
      />
      <View style={OwnerCardStyle.textCard}>
        <Text style={OwnerCardStyle.cardLabel} numberOfLines={1}>
          First Name
        </Text>
        <Text style={OwnerCardStyle.cardDetailText} numberOfLines={1}>
          {user?.firstName}
        </Text>
        <Text style={OwnerCardStyle.cardLabel} numberOfLines={1}>
          Last Name
        </Text>
        <Text style={OwnerCardStyle.cardDetailText} numberOfLines={1}>
          {user?.lastName}
        </Text>
      </View>
      <FavoriteButton isFavorite={favorite} onClickFavorite={onFavorite} />
    </View>
  )
}

export default OwnerCard
