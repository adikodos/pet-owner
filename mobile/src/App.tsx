import React from 'react'
import {ApolloClient, InMemoryCache, ApolloProvider} from '@apollo/client'
import {NavigationContainer} from '@react-navigation/native'
import {GRAPHQL_SERVER} from '@env'

import {RootScreenStack} from './navigations/RootStack'
import OwnerProvider from './contexts/OwnerContext'

const cache = new InMemoryCache()

const client = new ApolloClient({
  uri: GRAPHQL_SERVER,
  cache,
  defaultOptions: {watchQuery: {fetchPolicy: 'cache-and-network'}},
})

const App = () => {
  return (
    <ApolloProvider client={client}>
      <OwnerProvider>
      <NavigationContainer>
        <RootScreenStack />
      </NavigationContainer>
      </OwnerProvider>
    </ApolloProvider>
  )
}

export default App
