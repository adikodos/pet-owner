import {useEffect, useState} from 'react'
import storage from '../services/db/config'
import {FAVORITE_KEY_STORAGE} from '../constants/appConstants'
import { IOwner } from '../models/owner'

export interface FavoriteOwner {
  favorite: boolean
  onFavorite: () => Promise<void>
}

const useFavorite = (user: IOwner): FavoriteOwner => {
  const [favorite, setFavorite] = useState<boolean>(false)

  useEffect(() => {
    checkFavorite()
  }, [])

  const checkFavorite = async () => {
    const ownerFavorite = await isFavorite()
    setFavorite(ownerFavorite)
  }

  const onFavorite = async (): Promise<void> => {
    const favoriteUsers = await getFavoriteList()
    let newList = [...favoriteUsers]
    if (favoriteUsers.includes(user.id)) {
      setFavorite(false)
      newList.splice(favoriteUsers.indexOf(user.id))
    } else {
      setFavorite(true)
      newList.push(user.id)
    }

    storage.save({
      key: FAVORITE_KEY_STORAGE,
      data: {
        favoriteUsers: newList,
      },
      expires: null,
    })
  }

  const isFavorite = async (): Promise<boolean> => {
    const favoriteUsers = await getFavoriteList()
    const favUser = favoriteUsers.find(id => id === user.id)
    return favUser ? true : false
  }

  const getFavoriteList = async (): Promise<string[]> => {
    try {
      const {favoriteUsers} = await storage.load({
        key: FAVORITE_KEY_STORAGE,
        autoSync: true,
      })
      return [...favoriteUsers]
    } catch (err) {
      return []
    }
  }

  return {
    favorite,
    onFavorite,
  }
}

export default useFavorite
