import {gql} from '@apollo/client'

export const PET_OWNERS_QUERY = gql`
  query owners {
    owners {
      id
      firstName
      lastName
      name
      master
      cats {
        id
      }
    }
  }
`

export const OWNER_DETAILS_QUERY = gql`
  query owner($id: ID!) {
    owner(id: $id) {
      id
      name
      firstName
      lastName
      master
      cats {
        id
        name
        age
      }
    }
  }
`

export const MASTER_DETAILS_QUERY = gql`
  query master {
    master {
      id
      name
      firstName
      lastName
      master
    }
  }
`
