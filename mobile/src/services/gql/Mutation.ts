import {gql} from '@apollo/client'

export const SET_MASTER_MUTATION = gql`
  mutation updateMasterOwner($id: ID!) {
    updateMasterOwner(id: $id) {
      id
      name
      firstName
      lastName
      master
      cats {
        id
        name
        age
      }
    }
  }
`
