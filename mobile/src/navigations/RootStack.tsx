import React from 'react'
import {createNativeStackNavigator} from '@react-navigation/native-stack'
import Header from '../components/shared/Header'

import DetailScreen from './../screens/DetailScreen'
import HomeScreen from './../screens/HomeScreen'

export type RootStackParams = {
  Home: undefined
  Detail: {
    name: string
    id: string
  }
}

export const RootStack = createNativeStackNavigator<RootStackParams>()

export const RootScreenStack = () => {
  return (
    <RootStack.Navigator
      initialRouteName={'Home'}
      screenOptions={{
        headerShown: true,
      }}>
      <RootStack.Screen
        name={'Home'}
        component={HomeScreen}
        options={{
          header: () => <Header />,
        }}
      />
      <RootStack.Screen
        name={'Detail'}
        component={DetailScreen}
        options={{
          header: () => <Header showBackButton={true} />,
        }}
      />
    </RootStack.Navigator>
  )
}
