import {
    createContext,
    ReactNode,
    useContext,
    useEffect,
    useMemo,
    useReducer,
    useState
} from 'react'

import { OWNER_STATE } from '../constants/contextConstants'
import {
    getOwnersList,
    getOwnerDetails,
    getMasterDetails,
    setMasterDetails
} from '../contexts/actions/owner.action'
import { OwnerContextType, OwnerStateType, Sorting } from '../models/owner'
import { ownerReducer } from '../contexts/reducers/owner.reducer'
import { useApolloClient } from '@apollo/client'

export const OwnerContext = createContext<OwnerContextType>({
    ...OWNER_STATE,
    getOwners: async (sortBy: Sorting) => {},
    getOwner: async (id: number) => {},
    getMaster: async () => {},
    setMaster: async (id: number) => {}
})

const OwnerProvider = ({ children }: { children: ReactNode }) => {
    const [state, dispatch] = useReducer(ownerReducer, OWNER_STATE)
    const [ownerValue, setOwnerValue] = useState<OwnerStateType>(state)
    const client = useApolloClient()

    const actions = {
        getOwners: async (sortBy: Sorting) => await getOwnersList(client, dispatch, sortBy),
        getOwner: async (id: number) => await getOwnerDetails(client, dispatch, id),
        getMaster: async () => await getMasterDetails(client, dispatch),
        setMaster: async (id: number) => await setMasterDetails(client, dispatch, id)
    }

    useEffect(() => {
        const getMasterDetail = async () => {
            await getMasterDetails(client, dispatch)
        }
        getMasterDetail()
    }, [])

    useEffect(() => {
        setOwnerValue(state)
    }, [state])

    const provider = useMemo(() => (<OwnerContext.Provider
        value={{
            ...ownerValue,
            ...actions
        }}>
        {children}
    </OwnerContext.Provider>)
    , [ownerValue])

    return provider
}

export const useOwnerContext = () => useContext(OwnerContext)

export default OwnerProvider
