import { OWNER_STATE } from '../../constants/contextConstants'
import {
    getOwnersListRequest,
    getOwnerDetailsRequest,
    getMasterDetailsRequest,
    setMasterDetailsRequest
} from './../actions/owner.action'
import { type OwnerActions, OwnerActionTypes } from '../../models/action'

export const ownerReducer = (state = OWNER_STATE, action: OwnerActions) => {
    switch(action.type) {
        case OwnerActionTypes.GET_OWNERS:
            return getOwnersListRequest(state, action)
        case OwnerActionTypes.GET_OWNER_DETAIL:
            return getOwnerDetailsRequest(state, action)
        case OwnerActionTypes.GET_MASTER_DETAIL:
            return getMasterDetailsRequest(state, action)
        case OwnerActionTypes.SET_MASTER_DETAIL:
            return setMasterDetailsRequest(state, action)
        default:
            return state
    }
}