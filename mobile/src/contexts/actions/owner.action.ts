import { ApolloClient } from '@apollo/client'
import { Dispatch } from 'react'
import { MASTER_DETAILS_QUERY, OWNER_DETAILS_QUERY, PET_OWNERS_QUERY } from '../../services/gql/Query'
import { SET_MASTER_MUTATION } from '../../services/gql/Mutation'
import { OwnerActions, OwnerActionTypes, Payload } from '../../models/action'
import { OwnerStateType, Sorting } from '../../models/owner'
import { getSortedList } from '../../utils/getSortedList'

const assignNewObject = (state: OwnerStateType, newobj: Payload<OwnerStateType>): OwnerStateType => {
    return { ...state, ...newobj }
}

/* -------- list reducers actions -------- */
export const getOwnersListRequest = (state: OwnerStateType, action: OwnerActions): OwnerStateType => {
  const { payload } = action
  return assignNewObject(state, payload)
}
export const getOwnerDetailsRequest = (state: OwnerStateType, action: OwnerActions): OwnerStateType => {
  const { payload } = action
  return assignNewObject(state, payload)
}
export const getMasterDetailsRequest = (state: OwnerStateType, action: OwnerActions): OwnerStateType => {
  const { payload } = action
  return assignNewObject(state, payload)
}
export const setMasterDetailsRequest = (state: OwnerStateType, action: OwnerActions): OwnerStateType => {
  const { payload } = action
  return assignNewObject(state, payload)
}


/** GET OWNERS LIST */
const getOwnersList = async (client: ApolloClient<Object>, dispatch: Dispatch<OwnerActions>, sortBy: Sorting) => {
  dispatch({type: OwnerActionTypes.GET_OWNERS, payload: { loadingOwners: true, errorOwners: null }})
  try {
    const { data, error } = await client.query({query: PET_OWNERS_QUERY})
      if (data) {
          const sortedList = getSortedList(data?.owners, sortBy)
          dispatch({type: OwnerActionTypes.GET_OWNERS, payload: { loadingOwners: false, errorOwners: null, owners: sortedList, sortBy: sortBy }})
      }
      if (error) {
          dispatch({type: OwnerActionTypes.GET_OWNERS, payload: { loadingOwners: false, errorOwners: error?.message }})    
      }
  } catch (error: any) {
      dispatch({type: OwnerActionTypes.GET_OWNERS, payload: { loadingOwners: false, errorOwners: error as string }})
  }
}
/** GET OWNER DETAILS */
const getOwnerDetails = async (client: ApolloClient<Object>, dispatch: Dispatch<OwnerActions>, id: number) => {
    dispatch({type: OwnerActionTypes.GET_OWNER_DETAIL, payload: { loadingOwner: true, errorOwner: null }})
    try {
        const { data, error } = await client.query({query: OWNER_DETAILS_QUERY, variables: {id: id}})
        if (data) {
            dispatch({type: OwnerActionTypes.GET_OWNER_DETAIL, payload: { loadingOwner: false, errorOwner: null, owner: data.owner }})
        }
        if (error) {
            dispatch({type: OwnerActionTypes.GET_OWNER_DETAIL, payload: { loadingOwner: false, errorOwner: error?.message }})    
        }
    } catch (error: any) {
        dispatch({type: OwnerActionTypes.GET_OWNER_DETAIL, payload: { loadingOwner: false, errorOwner: error as string }})
    }
}
/** GET OWNER SET AS MASTER DETAILS */
const getMasterDetails = async (client: ApolloClient<Object>, dispatch: Dispatch<OwnerActions>) => {
    dispatch({type: OwnerActionTypes.GET_MASTER_DETAIL, payload: { loadingMaster: true, errorMaster: null }})
    try {
        const { data, error } = await client.query({query: MASTER_DETAILS_QUERY})
        if (data) {
            dispatch({type: OwnerActionTypes.GET_MASTER_DETAIL, payload: { loadingMaster: false, errorMaster: null, master: data.master }})
        }
        if (error) {
            dispatch({type: OwnerActionTypes.GET_MASTER_DETAIL, payload: { loadingMaster: false, errorMaster: error?.message }})    
        }
    } catch (error: any) {
        dispatch({type: OwnerActionTypes.GET_MASTER_DETAIL, payload: { loadingMaster: false, errorMaster: error as string }})
    }
}
/** OPERATION TO SET OWNER AS MASTER */
const setMasterDetails = async (client: ApolloClient<Object>, dispatch: Dispatch<OwnerActions>, id: number) => {
    dispatch({type: OwnerActionTypes.GET_MASTER_DETAIL, payload: { loadingMaster: true, errorMaster: null }})
    try {
        const { data, errors } = await client.mutate({mutation: SET_MASTER_MUTATION, variables: {id: id}})
        if (data) {
            dispatch({type: OwnerActionTypes.GET_MASTER_DETAIL, payload: { loadingMaster: false, errorMaster: null, master: data.updateMasterOwner }})
        }
        if (errors) {
            dispatch({type: OwnerActionTypes.GET_MASTER_DETAIL, payload: { loadingMaster: false, errorMaster: errors![0].message }})    
        }
    } catch (error: any) {
        dispatch({type: OwnerActionTypes.GET_MASTER_DETAIL, payload: { loadingMaster: false, errorMaster: error as string }})
    }
}

export { getOwnersList, getOwnerDetails, getMasterDetails, setMasterDetails }
