import React from 'react'
import {
  Alert,
  SafeAreaView,
  ScrollView,
  StatusBar,
  useColorScheme,
  View,
} from 'react-native'

import HomeScreenStyle from './../styles/HomeScreen'

import LabelText from './../components/labels/LabelText'
import Label from '../components/labels/Label'
import OwnerCard from '../components/cards/OwnerCard'
import PetList from './../components/lists/PetList'
import BottomActionButton from '../components/buttons/BottomActionButton'

import { useOwnerContext } from '../contexts/OwnerContext'

const DetailScreen = () => {
  const { owner, loadingOwner, setMaster } = useOwnerContext()

  const isDarkMode = useColorScheme() === 'dark'

  const onSetMaster = () => {
    setMaster(+owner!.id)
    Alert.alert(`${owner!.name} successfully mark as Master!`)
  }

  return (
    <SafeAreaView style={HomeScreenStyle.saveContainer}>
      <StatusBar
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
        backgroundColor={'#444'}
      />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={HomeScreenStyle.viewContainer}>
        {!loadingOwner && (
          <View style={[HomeScreenStyle.container, {paddingBottom: 54}]}>
            <Label>
              <LabelText label={'Owner Card'} />
            </Label>
            <OwnerCard user={owner!} />

            <Label style={{marginBottom: 8}}>
              <LabelText label={'Cats'} />
            </Label>
            {owner?.cats?.map((cat: any) => (
              <PetList key={cat.id} cat={cat} />
            ))}
          </View>
        )}
      </ScrollView>
      <BottomActionButton text="Make Master" onPress={onSetMaster} />
    </SafeAreaView>
  )
}

export default DetailScreen
