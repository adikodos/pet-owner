import React, {useCallback, useContext, useEffect, useRef, useState} from 'react'
import {
  View,
  ActivityIndicator,
  StatusBar,
  SafeAreaView,
  useColorScheme,
  RefreshControl,
  FlatList,
} from 'react-native'
import { NavigationContext, useFocusEffect } from '@react-navigation/native'

import HomeScreenStyle from './../styles/HomeScreen'

import OwnerList from '../components/lists/OwnerList'
import LabelText from '../components/labels/LabelText'
import Label from '../components/labels/Label'
import BottomPopup, {ListItem} from '../components/modals/BottomPopup'
import LabelDropDown from '../components/labels/LabelDropdown'

import {LIST_ITEM_HEIGHT, menuShort} from '../constants/appConstants'

import { useOwnerContext } from '../contexts/OwnerContext'

const HomeScreen = () => {
  const [refresh, setRefresh] = useState<boolean>(false)
  const [sortMenu, setSortMenu] = useState<ListItem[]>([...menuShort])
  let popupRef = useRef<any>(null)
  const isDarkMode = useColorScheme() === 'dark'
  const {owners, getOwners, sortBy, loadingOwners, getOwner} = useOwnerContext()    
  const navigation = useContext(NavigationContext)

  useEffect(() => {
    getOwners(sortBy)
  }, [])
  
  useFocusEffect(useCallback(() => {
    const idSortSelected = sortMenu.findIndex(menu => menu.checked === true)
    onRefreshShort(idSortSelected + 1)
  }, []))

  const getCheckedName = async () => {
    const nameMenuSelected = menuShort.find(menu => menu.checked)!.name
    await getOwners(nameMenuSelected)
  }

  const onShowPopup = () => {
    popupRef.current.show()
  }
  const onClosePopup = () => {
    popupRef.current.hide()
  }

  const onRefreshList = useCallback(async () => {
    setRefresh(true)
    await getCheckedName()
    setRefresh(false)
  }, [])

  const onRefreshShort = (id: number | string) => {
    sortMenu.forEach(menu => {
      menu.checked = menu.id === id ? true : false
      return menu
    })
    setSortMenu(sortMenu)
    getCheckedName()
  }

  const onNavigateToDetail = async (id: string) => {
    await getOwner(+id)
    navigation?.navigate('Detail', {
      id: id,
    })
  }

  const labelHeader = () => (
    <Label style={{marginBottom: 24}}>
      <LabelText label={'Owners List'} />
      <LabelDropDown
        selected={sortBy}
        onShowPopup={onShowPopup}
      />
    </Label>
  )

  if (loadingOwners) {
    return <ActivityIndicator size={'large'} style={{marginTop: 24}} />
  }

  return (
    <SafeAreaView style={HomeScreenStyle.saveContainer}>
      <StatusBar
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
        backgroundColor={'#444'}
      />
      <View style={HomeScreenStyle.viewContainer}>
        <FlatList
          contentInsetAdjustmentBehavior={'automatic'}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={HomeScreenStyle.container}
          ListHeaderComponent={labelHeader}
          refreshControl={
            <RefreshControl refreshing={refresh} onRefresh={onRefreshList} />
          }
          data={owners}
          renderItem={({item}: {item: any}) => (
            <OwnerList key={item?.id} user={item} goToDetail={onNavigateToDetail} />
          )}
          extraData={owners}
          keyExtractor={item => item.id.toString()}
          getItemLayout={(data, index) => (
            {length: LIST_ITEM_HEIGHT, offset: LIST_ITEM_HEIGHT * index, index}
          )}
        />
      </View>
      <BottomPopup
        title="Sort By"
        list={sortMenu}
        ref={popupRef}
        onTouchOutside={onClosePopup}
        onSelectSort={onRefreshShort}
      />
    </SafeAreaView>
  )
}

export default HomeScreen
