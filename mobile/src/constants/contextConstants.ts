import { OwnerStateType } from '../models/owner'

export const OWNER_STATE: OwnerStateType = {
    owners: [],
    owner: null,
    master: null,
    loadingOwners: false,
    loadingOwner: false,
    loadingMaster: false,
    errorOwners: null,
    errorOwner: null,
    errorMaster: null,
    sortBy: 'Name'
}