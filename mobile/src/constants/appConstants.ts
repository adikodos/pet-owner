import {Dimensions} from 'react-native'
import {ListItem} from '../components/modals/BottomPopup'

export const menuShort: ListItem[] = [
  {
    id: 1,
    name: 'Name',
    checked: true,
  },
  {
    id: 2,
    name: 'Number of Cats',
    checked: false,
  },
]

export const deviceHeight = Dimensions.get('window').height

export const FAVORITE_KEY_STORAGE = 'favoriteState'
export const BACKGROUND_COLOR = '#F8F8FF'

export const LIST_ITEM_HEIGHT = 80
export const ITEM_CONTAINER_HEIGHT = 64
