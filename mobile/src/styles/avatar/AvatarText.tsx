import {StyleSheet} from 'react-native'

const AvatarTextStyle = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    textAlign: 'center',
    textAlignVertical: 'center',
  },
})

export default AvatarTextStyle
