import {StyleSheet} from 'react-native'
import { BACKGROUND_COLOR, ITEM_CONTAINER_HEIGHT } from '../../constants/appConstants'

const HeaderStyle = StyleSheet.create({
  headerTitle: {
    color: '#404040',
    fontSize: 14,
    lineHeight: 18,
    fontWeight: '500',
    letterSpacing: 0.02,
    marginLeft: 16,
  },
  avatarContainer: {
    backgroundColor: '#FFFFFF',
    borderColor: '#36A388',
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  avatarLabel: {
    color: '#36A388',
    fontSize: 15,
    lineHeight: 18,
    fontWeight: '600',
    alignItems: 'center',
  },
  container: {
    height: ITEM_CONTAINER_HEIGHT,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: BACKGROUND_COLOR,
  },
  backButtonContainer: {
    flex: 0,
    marginLeft: 24,
    justifyContent: 'center',
    width: 50,
    height: ITEM_CONTAINER_HEIGHT,
  },
  imageBackButton: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: 13,
    height: 12,
  },
  headerTitleContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
})

export default HeaderStyle
