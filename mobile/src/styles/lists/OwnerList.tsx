import {StyleSheet} from 'react-native'
import { ITEM_CONTAINER_HEIGHT } from '../../constants/appConstants'

const OwnerListStyle = StyleSheet.create({
  listRow: {
    flexDirection: 'row',
    paddingTop: 12,
    paddingLeft: 16,
    paddingRight: 26,
    paddingBottom: 12,
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    borderRadius: 12,
    marginBottom: 16,
    height: ITEM_CONTAINER_HEIGHT,
  },
  rowStart: {
    justifyContent: 'flex-start',
    flex: 1,
    marginLeft: 16,
  },
  titleOwner: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  ownerText: {
    color: '#404040',
    fontSize: 14,
    lineHeight: 20,
    fontWeight: '500',
  },
  boxShadow: {
    elevation: 32,
    shadowColor: 'rgba(110, 113, 145, 0.22)',
  },
  avatarLabel: {
    fontSize: 15,
    fontWeight: '600',
    lineHeight: 18
  },
})

export default OwnerListStyle
