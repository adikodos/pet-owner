import {StyleSheet} from 'react-native'

const PetList = StyleSheet.create({
  listPet: {
    flexDirection: 'row',
    paddingVertical: 17.5,
    paddingLeft: 16,
    paddingRight: 32,
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    borderRadius: 12,
    marginBottom: 8,
  },
  petCard: {
    justifyContent: 'flex-start',
    flex: 1,
  },
  titlePet: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  petText: {
    color: '#404040',
    fontSize: 14,
    lineHeight: 20,
    fontWeight: '500',
    marginBottom: 8,
  },
  petSubtext: {
    color: '#A1AFC3',
    fontSize: 12,
    lineHeight: 15,
    fontWeight: '500',
  },
})

export default PetList
