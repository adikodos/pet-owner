import {StyleSheet} from 'react-native'

const BottomActionButtonStyle = StyleSheet.create({
  btnContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    right: 10,
    left: 10,
    position: 'absolute',
    bottom: 10,
  },
  btnAction: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#36A388',
    paddingVertical: 12,
    borderRadius: 8,
    width: 268,
  },
  btnText: {
    fontSize: 18,
    lineHeight: 24,
    fontWeight: '500',
    alignItems: 'center',
    color: '#FCFCFC',
  },
})

export default BottomActionButtonStyle
