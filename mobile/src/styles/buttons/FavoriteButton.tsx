import {StyleSheet} from 'react-native'

const FavoriteButtonStyle = StyleSheet.create({
  rowEnd: {
    justifyContent: 'flex-end',
    marginRight: 19,
  },
  iconDetail: {
    justifyContent: 'flex-end',
  },
})

export default FavoriteButtonStyle
