import {StyleSheet} from 'react-native'
import {deviceHeight} from '../../constants/appConstants'

const ButtomPopupStyle = StyleSheet.create({
  modalContainer: {
    flex: 1,
    backgroundColor: '#000000AA',
    justifyContent: 'flex-end',
  },
  popupContainer: {
    backgroundColor: '#FFFFFF',
    width: '100%',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    maxHeight: deviceHeight * 0.4,
  },
  separator: {
    opacity: 0.1,
    backgroundColor: '#182E44',
    height: 1,
  },
  titlePopupContainer: {
    alignItems: 'center',
    borderBottomColor: '#182E44',
    borderBottomWidth: 1,
  },
  titlePopup: {
    color: '#182E44',
    fontSize: 21,
    lineHeight: 24,
    fontWeight: '500',
    margin: 15,
  },
  itemTouchable: {
    height: 50,
    flex: 1,
    alignItems: 'flex-start',
    marginLeft: 24,
    flexDirection: 'row',
  },
  itemContainer: {
    flex: 1,
    alignItems: 'flex-start',
    flexDirection: 'row',
  },
  itemImage: {
    width: 20,
    height: 20,
    alignSelf: 'center',
    marginRight: 9,
  },
  itemText: {
    fontSize: 15,
    fontWeight: '400',
    marginTop: 15,
    marginBottom: 15,
    color: '#182E44',
    lineHeight: 20,
  },
  touchableView: {
    flex: 1,
    width: '100%'
  },
  contentList: {
    paddingBottom: 10,
  },
})

export default ButtomPopupStyle
