import {StyleSheet} from 'react-native'
import {BACKGROUND_COLOR} from '../constants/appConstants'

const HomeScreenStyle = StyleSheet.create({
  container: {
    backgroundColor: BACKGROUND_COLOR,
    paddingHorizontal: 24,
    flexDirection: 'column',
    marginTop: 24,
    paddingBottom: 24,
  },
  saveContainer: {
    backgroundColor: BACKGROUND_COLOR,
    flex: 1,
  },
  viewContainer: {
    backgroundColor: BACKGROUND_COLOR,
    flex: 1,
    flexDirection: 'column',
  },
})

export default HomeScreenStyle
