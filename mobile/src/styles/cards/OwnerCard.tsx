import {StyleSheet} from 'react-native'

const OwnerCardStyle = StyleSheet.create({
  cardContainer: {
    flexDirection: 'row',
    paddingVertical: 19,
    paddingLeft: 16,
    paddingRight: 18,
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    borderRadius: 12,
    marginBottom: 8,
  },
  textCard: {
    justifyContent: 'flex-start',
    flex: 1,
    marginLeft: 40,
  },
  cardLabel: {
    color: '#A1AFC3',
    fontSize: 12,
    lineHeight: 15,
    fontWeight: '500',
    marginBottom: 2,
  },
  cardDetailText: {
    color: '#404040',
    fontSize: 14,
    lineHeight: 20,
    fontWeight: '500',
    marginBottom: 10,
  },
  avatarLabel: {
    fontSize: 20,
    fontWeight: '600',
    lineHeight: 24,
  },
})

export default OwnerCardStyle
