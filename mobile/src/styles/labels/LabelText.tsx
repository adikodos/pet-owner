import {StyleSheet} from 'react-native'

const LabelTextStyle = StyleSheet.create({
  labelText: {
    flex: 1,
    justifyContent: 'flex-start',
    fontSize: 14,
    lineHeight: 20,
    fontWeight: '500',
    color: '#A1AFC3',
  },
})

export default LabelTextStyle
