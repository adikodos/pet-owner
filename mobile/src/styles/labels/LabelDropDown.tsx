import {StyleSheet} from 'react-native'

const LabelDropDownStyle = StyleSheet.create({
  container: {
    flex: 0,
    justifyContent: 'flex-end',
    flexDirection: 'row',
    alignItems: 'center',
  },
  label: {
    flex: 0,
    fontSize: 12,
    lineHeight: 15,
    fontWeight: '500',
    color: '#92929D',
  },
  icon: {width: 10, height: 7, marginLeft: 8},
})

export default LabelDropDownStyle
