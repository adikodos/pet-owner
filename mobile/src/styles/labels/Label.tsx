import {StyleSheet} from 'react-native'

const LabelStyle = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 16,
  },
})

export default LabelStyle
