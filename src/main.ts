import { ValidationPipe } from '@nestjs/common'
import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  app.useGlobalPipes(new ValidationPipe())
  app.enableCors({credentials: true, origin: ['https://pet-master-test.herokuapp.com/graphql']})

  await app.listen(process.env.PORT || 3000)
  console.log(`Application is running at: ${await app.getUrl()}`)
}
bootstrap()
