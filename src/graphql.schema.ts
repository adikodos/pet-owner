
/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */

export class CreateCatInput {
    name: string;
    age: number;
    ownerId: string;
}

export class CreateOwnerInput {
    firstName: string;
    lastName: string;
    age?: Nullable<number>;
}

export abstract class IQuery {
    abstract cats(): Nullable<Nullable<Cat>[]> | Promise<Nullable<Nullable<Cat>[]>>;

    abstract cat(id: string): Nullable<Cat> | Promise<Nullable<Cat>>;

    abstract owners(): Nullable<Nullable<Owner>[]> | Promise<Nullable<Nullable<Owner>[]>>;

    abstract owner(id: string): Nullable<Owner> | Promise<Nullable<Owner>>;

    abstract master(): Nullable<Owner> | Promise<Nullable<Owner>>;
}

export abstract class IMutation {
    abstract createCat(createCatInput?: Nullable<CreateCatInput>): Cat | Promise<Cat>;

    abstract createOwner(createOwnerInput?: Nullable<CreateOwnerInput>): Owner | Promise<Owner>;

    abstract updateMasterOwner(id: string): Owner | Promise<Owner>;
}

export abstract class ISubscription {
    abstract catCreated(): Nullable<Cat> | Promise<Nullable<Cat>>;

    abstract ownerCreated(): Nullable<Owner> | Promise<Nullable<Owner>>;
}

export class Cat {
    id: string;
    name: string;
    age?: Nullable<number>;
    owner?: Nullable<Owner>;
}

export class Owner {
    id: string;
    name?: Nullable<string>;
    firstName: string;
    lastName: string;
    age?: Nullable<number>;
    master: boolean;
    cats?: Nullable<Cat[]>;
}

type Nullable<T> = T | null;
