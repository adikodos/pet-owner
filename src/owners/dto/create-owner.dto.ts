import { IsNotEmpty, IsOptional, Min } from 'class-validator'
import { CreateOwnerInput } from '../../graphql.schema'

export class CreateOwnerDto extends CreateOwnerInput {
    @IsNotEmpty()
    firstName: string

    @IsNotEmpty()
    lastName: string

    @Min(1)
    @IsOptional()
    age: number
}
