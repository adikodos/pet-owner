import { IsNotEmpty } from 'class-validator'

export class UpdateMasterOwnerDto {
    @IsNotEmpty()
    id: string
}
