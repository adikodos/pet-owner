import { Module } from '@nestjs/common'
import { CatsService } from 'src/cats/cats.service'
import { OwnerCatResolver } from './owner-cat.resolve'
import { OwnersResolver } from './owners.resolver'
import { OwnersService } from './owners.service'

@Module({
  providers: [OwnersService, CatsService, OwnersResolver, OwnerCatResolver],
})
export class OwnersModule {}
