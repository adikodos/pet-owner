import { Injectable } from '@nestjs/common'
import { Owner } from '../graphql.schema'
import db from './../db'

@Injectable()
export class OwnersService {

  create(owner: Owner): Owner {
    owner.id = String(db.owners.length + 1)
    owner.name = owner.firstName + ' ' + owner.lastName
    owner.master = false
    db.owners.push(owner)
    return owner
  }

  findAll(): Owner[] {
    return db.owners
  }

  findOneById(id: string): Owner {
    return db.owners.find(owner => owner.id === id)
  }

  findMaster(): Owner {
    return db.owners.find(owner => owner.master === true)
  }

  setMasterById(id: string): Owner {
    let owner: Owner
    db.owners.forEach(ow => {
      if (ow.id == id) {
        owner = ow
        return ow.master = true
      }
      return ow.master = false
    })
    return owner
  }
}
