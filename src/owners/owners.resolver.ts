import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql'
import { PubSub } from 'graphql-subscriptions'
import { Owner } from '../graphql.schema'
import { OwnersService } from './owners.service'

const pubsub = new PubSub()

@Resolver('Owner')
export class OwnersResolver {
  constructor(private readonly ownersService: OwnersService) {}

  @Query('owners')
  async getOwners() {
    return this.ownersService.findAll()
  }

  @Query('owner')
  async findOneById(@Args('id') id: string): Promise<Owner> {
    return this.ownersService.findOneById(id)
  }
  
  @Query('master')
  async findMaster(): Promise<Owner> {
    return this.ownersService.findMaster()
  }

  @Mutation('createOwner')
  async create(
    @Args('createOwnerInput')
    args: any
  ): Promise<Owner> {
    const createdOwner = await this.ownersService.create(args)
    pubsub.publish('ownerCreated', { ownerCreated: createdOwner })
    return createdOwner
  }
  
  @Mutation('updateMasterOwner')
  async setMaster(@Args('id') id: string): Promise<Owner> {
    const masterOwner = await this.ownersService.setMasterById(id)
    return masterOwner
  }

  @Subscription('ownerCreated')
  ownerCreated() {
    return pubsub.asyncIterator('ownerCreated')
  }
}
