import { Owner } from './../graphql.schema'

const owners: Owner[] = [
    {
        id: '1',
        name: 'Jon Doe',
        firstName: 'Jon',
        lastName: 'Doe',
        age: 5,
        master: true,
        cats: []
    }
]

export default owners
