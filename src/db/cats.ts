import { Cat } from "src/graphql.schema";

const cats: Array<Cat & {ownerId?: string}> = [
    {
        id: '1',
        name: 'Cat',
        age: 5,
        ownerId: '1'
    }
]

export default cats
