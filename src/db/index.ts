import cats from './cats'
import owners from './owners'

const db = {
    cats,
    owners
}

export default db
