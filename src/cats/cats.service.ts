import { Injectable } from '@nestjs/common'
import { Cat } from '../graphql.schema'
import db from './../db'

@Injectable()
export class CatsService {

  create(cat: Cat ): Cat {
    cat.id = String(db.cats.length + 1)
    db.cats.push(cat)
    return cat
  }

  findAll(): Cat[] {
    return db.cats
  }

  findOneById(id: string): Cat {
    return db.cats.find(cat => cat.id === id)
  }

  findCatByOwnerId(id: string): Array<Cat & { ownerId?: string }> {
    return db.cats.filter(cat => cat.ownerId === id)
  }
}
