import { UseGuards } from '@nestjs/common'
import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql'
import { PubSub } from 'graphql-subscriptions'
import { Cat } from '../graphql.schema'
import { CatsGuard } from './cats.guard'
import { CatsService } from './cats.service'
import { CreateCatDto } from './dto/create-cat.dto'

const pubsub = new PubSub()

@Resolver('Cat')
export class CatsResolver {
  constructor(private readonly catsService: CatsService) {}

  @Query('cats')
  @UseGuards(CatsGuard)
  async getCats() {
    return this.catsService.findAll()
  }

  @Query('cat')
  async findOneById(
    @Args('id')
    id: string,
  ): Promise<Cat> {
    return this.catsService.findOneById(id)
  }

  @Mutation('createCat')
  async create(
    @Args('createCatInput')
    args: CreateCatDto
  ): Promise<Cat> {
    const createdCat = await this.catsService.create({ ...args, id: null})
    pubsub.publish('catCreated', { catCreated: createdCat })
    return createdCat
  }

  @Subscription('catCreated')
  catCreated() {
    return pubsub.asyncIterator('catCreated')
  }
}
