import { IsNotEmpty, Min } from 'class-validator'
import { CreateCatInput} from '../../graphql.schema'

export class CreateCatDto extends CreateCatInput {
  @IsNotEmpty()
  name: string

  @Min(1)
  age: number
}
