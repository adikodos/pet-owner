import { Module } from '@nestjs/common'
import { OwnersService } from 'src/owners/owners.service'
import { CatOwnerResolver } from './cat-owner.resolver'
import { CatsResolver } from './cats.resolver'
import { CatsService } from './cats.service'

@Module({
  providers: [CatsService, OwnersService, CatsResolver, CatOwnerResolver],
})
export class CatsModule {}
